<?php

namespace App\Action\Log;

use App\Repository\LogRepositoryInterface;

class GetLogsByLevelResponse
{
    private LogRepositoryInterface $logRepository;

    public function __construct(LogRepositoryInterface $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    public function getLogs($level): array
    {
        return $this->logRepository->getLogsByLevel($level);
    }
}
