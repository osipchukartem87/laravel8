<?php

namespace App\Action\Log;

use App\Repository\LogRepositoryInterface;

class GetLogsStatisticAction
{
    private LogRepositoryInterface $logRepository;

    public function __construct(LogRepositoryInterface $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    public function execute(): GetLogsStatisticResponse
    {
        return new GetLogsStatisticResponse($this->logRepository);
    }
}
