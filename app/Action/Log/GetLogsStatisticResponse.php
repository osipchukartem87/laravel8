<?php

namespace App\Action\Log;

use App\Repository\LogRepositoryInterface;

class GetLogsStatisticResponse
{
    private LogRepositoryInterface $logRepository;

    public function __construct(LogRepositoryInterface $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    public function getLogs(): array
    {
        return $this->logRepository->getLogsStatistic();
    }
}
