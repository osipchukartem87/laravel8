<?php

namespace App\Action\Log;

use App\Repository\LogRepositoryInterface;

class GetAllLogsAction
{
    private LogRepositoryInterface $logRepository;

    public function __construct(LogRepositoryInterface $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    public function execute(): GetAllLogsResponse
    {
        return new GetAllLogsResponse($this->logRepository);
    }
}
