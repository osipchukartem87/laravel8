<?php

namespace App\Http\Controllers;

use App\Action\Log\GetAllLogsAction;
use App\Action\Log\GetLogsByLevelAction;
use App\Action\Log\GetLogsStatisticAction;
use App\Repository\LogRepositoryInterface;
use Illuminate\Http\Request;

class LogController extends Controller
{
    private LogRepositoryInterface $logRepository;

    public function __construct(LogRepositoryInterface $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    public function index(): array
    {
        $response = (new GetAllLogsAction($this->logRepository))->execute();
        return $response->getLogs();
    }

    public function getLogsByLevel(Request $request, $level): array
    {
        $response = (new GetLogsByLevelAction($this->logRepository))->execute();
        return $response->getLogs(array($level));
    }

    public function statistic(): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $response = (new GetLogsStatisticAction($this->logRepository))->execute();
        $data = [];
        foreach($response->getLogs() as $key => $value) {
            $data[] = $value['level'];
        }

        return  view('logs', ['data' => array_count_values($data)]);
    }
}
