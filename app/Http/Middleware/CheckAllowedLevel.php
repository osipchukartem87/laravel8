<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class CheckAllowedLevel
{
    public function handle(Request $request, Closure $next)
    {
        $allowedLevel = ['info', 'warning', 'error', 'debug', 'critical', 'alert', 'emergency', 'notice', 'statistic'];
        if(!in_array($request->level, $allowedLevel)) {
            return abort(Response::HTTP_FORBIDDEN);
        }
        return $next($request);
    }
}
