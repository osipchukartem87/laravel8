<?php

namespace App\Repository;

interface LogRepositoryInterface
{
    public function findAll(): array;
    public function getLogsByLevel(array $level): array;
    public function getLogsStatistic(): array;
}
