<?php

namespace App\Repository;

use App\Models\Log;

class LogRepository implements LogRepositoryInterface
{

    public function findAll(): array
    {
        return Log::all()->toArray();
    }

    public function getLogsByLevel(array $level): array
    {
        return  Log::whereIn('level', $level)->get()->toArray();
    }

    public function getLogsStatistic(): array
    {
        return Log::all()->where('level')->toArray();
    }
}
