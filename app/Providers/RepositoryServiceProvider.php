<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\LogRepositoryInterface;
use App\Repository\LogRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(LogRepositoryInterface::class, LogRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
