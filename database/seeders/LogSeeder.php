<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Log;

class LogSeeder extends Seeder
{

    public function run()
    {
        Log::factory(rand(10, 100))->create();
    }
}
