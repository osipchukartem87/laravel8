<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Log Viewer</title>
</head>
<body>
    <div>'[
        @foreach(['info', 'warning', 'error', 'debug', 'critical', 'alert', 'emergency', 'notice'] as $value)
            @if(array_key_exists($value, $data))
                <span>{{ $value }} => {{ $data[$value] }},</span>
            @else
                <span>{{ $value }} => 0,</span>
            @endif
        @endforeach
        ]'
    </div>
</body>
</html>
